/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.testdatabase;

/**
 *
 * @author BenZ
 */
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InsertUser {

    public static void main(String[] args) {
        Connection c = null;
        Statement stmt = null;
        String file = "user.db";

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + file);
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "INSERT INTO user (username,password)"
                    + "VALUES ('Paul', 'password' );";
            stmt.executeUpdate(sql);
            
            sql = "INSERT INTO user (username,password)"
                    + "VALUES ('Benz', 'password' );";
            stmt.executeUpdate(sql);
            
            c.commit();
            stmt.close();
            c.close();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("Records created successfully");

    }

}
