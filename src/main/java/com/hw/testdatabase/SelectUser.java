/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.testdatabase;

/**
 *
 * @author BenZ
 */
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SelectUser {

    public static void main(String[] args) {
        Connection c = null;
        Statement stmt = null;
        String file = "user.db";

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + file);
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM user;");

            while (rs.next()) {
                //System.out.println("asdasd");
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");

                System.out.println("[ID : " + id + ", User Name : " + username
                        + ", Password : " + password + "]");

            }
            /*while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String address = rs.getString("address");
                float salary = rs.getFloat("salary");

                System.out.println("ID = " + id);
                System.out.println("NAME = " + name);
                System.out.println("AGE = " + age);
                System.out.println("ADDRESS = " + address);
                System.out.println("SALARY = " + salary);
                System.out.println();
            }*/
            rs.close();
            stmt.close();
            c.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SelectUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Operation done successfully");

    }

}
