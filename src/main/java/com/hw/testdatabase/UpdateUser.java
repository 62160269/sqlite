/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.testdatabase;

/**
 *
 * @author BenZ
 */
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UpdateUser {

    public static void main(String[] args) {
        Connection c = null;
        Statement stmt = null;
        String file = "user.db";

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + file);
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "UPDATE user set 'password' = '1234' where id=6;";
            stmt.executeUpdate(sql);
            c.commit();

            ResultSet rs = stmt.executeQuery("SELECT * FROM user;");

            while (rs.next()) {
                //System.out.println("asdasd");
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");

                System.out.println("[ID : " + id + ", User Name : " + username
                        + ", Password : " + password + "]");
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UpdateUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UpdateUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Operation done successfully");

    }

}
